Title: Stax 3.0, nouvelle version, nouvelle recette !
Date: 2021-12-21
Category: News
Tags: news, Stax 3
Slug: stax-3.0
Author: Félix David
Lang: fr

Depuis plusieurs mois le développement de Stax se concentre sur une nouvelle architecture plus souple et plus rapide.
Cette refonte du Core va permettre une évolution plus robuste dans les mois à venir.
*Si vous vous demandez ce qu'est Stax vous pouvez jeter un oeil à [l'article sur la mise en libre](https://superprod.gitlab.io/stax_suite/stax/stax-open-source.html).*

<script src="http://vjs.zencdn.net/4.0/video.js"></script>

<video id="pelican-installation" class="video-js vjs-default-skin" controls
preload="auto" width="683"
data-setup="{}">
<source src="videos/Stax_SH_Workflow1.webm" type='video/webm'>
</video>

## Dites m'en plus !

Dans les versions *2.x*, la création d'un montage passait obligatoirement par l'importation d'un fichier de montage respectant une structure bien particulière.
Bien que ce système utilisait [OpenTimelineIO](https://opentimelineio.readthedocs.io/en/latest/), il était très contraignant.
Avec les versions *3.x*, il est possible d'en faire la *review* sans avoir à conformer son fichier de montage.
La liberté est totale et permet de se concentrer sur la façon la plus pertinente de représenter sa production pour une *review* efficace.

## Cela veut-il dire qu'il n'y plus d'utilisation d'OpenTimelineIO possible ?

Au contraire ! La gestion de l'importation/exportation d'un montage pour Blender a été déplacée dans un add-on dédié : le [VSE IO](https://gitlab.com/superprod/stax_suite/vse_io).
Cet add-on est entièrement utilisable avec Stax, mais peut également l'être indépendamment.

## Et la vitesse ?

La suppression de l'architecture interne qui forçait l'usage d'une timeline avec une structure figée dans les versions *2.x* impliquait dans le même temps un ralentissement de tous les processus.
Maintenant, Stax repose uniquement sur la timeline du VSE!
Par ailleurs, des optimisations concernant les fonctionnalités déjà existantes ont été réalisées, la vitesse est donc au rendez-vous! Même sur de très gros montages !

## Donc aucune nouvelle feature ?

Un tout nouveau système de *review* a été mis en place pour permettre l'usage de Stax hors de tout *pipeline*. Il permet de publier et lier des *reviews* à partir du disque local ou d'un serveur sans avoir besoin d'un gestionnaire de production (voir la vidéo en tête d'article).

## Une nouvelle dynamique

Stax était jusqu'ici porté par SuperProd Studio, en grande majorité développé par Félix David. Il s'avère qu'il quitte SPS pour être responsable du *pipeline* chez Normaal Animation. Stax va donc être utilisé dans un studio de plus en tant qu'outil de *review*.

D'autres studios étudient très sérieusement l'utilisation de Stax pour leurs artistes, séduits par les nombreux avantages de l'outil, et de nouvelles collaborations vont certainement apparaitre sur des points précis du projet.

[CGWire](https://www.cg-wire.com/) a également commencé à travailler à un lien direct entre Stax et leur gestionnaire de production, [Kitsu](https://www.cg-wire.com/en/kitsu.html), à suivre donc !

## Vous souhaitez rejoindre l'aventure ?

- Rejoignez notre [Discord](https://discord.gg/8ZNx7Vz) 
- Découvrez la [Documentation](https://superprod.gitlab.io/stax_suite/stax/docs/) (en anglais)
- Lisez le document de [Contributing](https://gitlab.com/superprod/stax_suite/stax/-/blob/master/CONTRIBUTING.md)
- Fouillez le [GitLab](https://gitlab.com/superprod/stax_suite/stax/)

Actuellement Stax est fourni sous la forme d'un [Application Template](https://docs.blender.org/manual/en/latest/advanced/app_templates.html).

## En vidéo, c'est toujours mieux

Pour avoir une idée plus précise de l'état actuel de Stax et des besoins auxquels il répond, vous pouvez visionner notre intervention au [RADI 2021](https://youtu.be/MY2fEUNfJKE?t=1010).