.. _`known limitations`:

=================
Known Limitations
=================

Due to VSE 's python API lack of features and Stax's development legacy, all Blender's features aren't accessible to avoid unstability.
We are trying to make it work by contributing to Blender's core VSE code and making Stax's core more versatile, feel free to declare yourself if you want to help on this parts.

------------------
Link/Append Scenes
------------------
`Link/Append <https://docs.blender.org/manual/en/latest/files/linked_libraries/link_append.html#link-append>`_ scenes to load an editing.

``bpy.ops.sequencer.meta_separate()`` is currently used in several places and requires a context override, need a ``meta.separate()`` python function.