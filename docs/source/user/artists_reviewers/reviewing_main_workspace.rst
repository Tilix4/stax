.. _`reviewing-main-workspace`:

===============================
Reviewing in the Main workspace
===============================

This workspace allows you to execute the four steps of a review:

#. View_
#. Compare_
#. Edit_
#. Publish_

.. important::
   As Stax is mainly used by artists working in studios, 
   the features explanation is based on production examples.


Navigation
==========

The Sequence Editor
-------------------

The Sequence Editor is divided into two parts:

-  **The Preview**, with your display.

-  **The Editor**, with your timeline, composed of:

   -  **A header** with the time (seconds + frame), and your
      current time.
   -  **A timeline**, with a line for each track and the different medias in time.


You’ve got several ways to navigate in Blender's Sequencer
Editor:

-  `Buttons to play, stop...`_
-  `Use the mouse`_
-  `Use the elevators widgets`_


Buttons to play, stop...
------------------------

Use the ``Spacebar`` to play and stop.

.. video:: ../images/PlayButtons.mp4
   :autoplay: yes
   :loop: yes
   :controls: no

   Use of navigation buttons

Use the mouse
-------------

In the Editor’s header
^^^^^^^^^^^^^^^^^^^^^^

-  **Left-click** in the **timeline** to move the
   playhead
-  **Left-click & drag** in the **timeline** to
   scroll in time

   -  Blender uses an infinite drag system

.. video:: ../images/ScrollInTime.mp4
   :autoplay: yes
   :loop: yes
   :controls: no

   In the Editor's header

.. note::

   You can see that the status of your versions disappear when
   you move in time, it’s for performance reasons.

In the Editor’s timeline
^^^^^^^^^^^^^^^^^^^^^^^^

-  **Middle-click & drag** to move the timeline view
   left/right or bottom/top
-  Use the **mouse wheel** to zoom in and out

.. video:: ../images/DragInTime.mp4
   :autoplay: yes
   :loop: yes
   :controls: no

   In the Editor's timeline

.. note::

   You can do a ``Left-click & drag`` in the timeline to get a
   selection box but **there’s no need to select anything in
   Stax**.

Use the elevators widgets
-------------------------

-  Horizontal one will let you zoom and move in time
-  Vertical one will let your zoom and move in the tracks.

.. _view:

View
====

About the medias in the timeline
----------------------------------

You can see all your medias with their notes and reviewing status in the timeline.
You can see the currently displayed media by a white highlight in the timeline.

.. figure:: /_static/images/StaxLoaded.png
   :alt: All your medias and tracks

   All your medias and tracks

.. important::

   The displayed version of a media is **always the last one**.



Status
------

Each media has its reviewing status indicated:

- In the tool header:

   .. figure:: ../images/Toolbar_Status.png
         :alt: Toolbar status
         :align: center
         
         Toolbar status

- Over the media:
   -  A green line indicates it's been approved,
   -  A red line indicates it's been rejected (i.e retakes have to be done),
   -  An orange line indicates a media is waiting a review.

   .. figure:: ../images/Medias_ApprovedRejected.png
      :alt: Left media is approved, right one is rejected
      :scale: 50 %

      Left media is rejected, right one is approved


View the annotations
--------------------

One of the main features of Stax is to create and display
drawings (a.k.a image annotations) over your medias.

When you link the reviews, drawings display is activated by default.

In the ``Notes`` panel you can see published image annotations of a note and click on them to go to the related frame.

.. figure:: /_static/images/Notes_ImageAnnotations.jpg
   :alt: Notes panel Image Annotations clickable references

   Notes panel Image Annotations clickable references

In the timeline, you can see that the medias with annotations have a greenish
color and ``stax.`` prefix, the other a blueish one. 
The frames which have been drawn over are indicated by markers in the sequencer.

.. figure:: /_static/images/ImageAnnotation_SequencerMarker.png
   :alt: Left media has annotations, right one doesn't

   Left media has annotations, right one doesn't

To show/hide drawings of the selected sequences, use the **Show**,
**Hide** buttons. To show/hide for all the sequences with reviews hold ``shift`` when clicking.

See also `how to draw annotations`_.

.. _`how to draw annotations`: `Draw`_

.. figure:: /_static/images/Drawings_ShowHide.png
   :alt: Drawings On / Off

   Drawings On / Off

.. _emphasize drawings:

Emphasize Drawings
------------------
To see the drawings better, you can change the whiteness of the media in the preview. ``0%`` shows the media untouched, while ``100%`` shows it fully white.

.. figure:: ../images/Preview_EmphasizeDrawings.png
   :alt: Emphasize Drawings

   Emphasize Drawings

.. _`view the comments`:

Sidebar
-------
To show/hide the sidebar, use the **Show/Hide** ``INFO`` button. It'll open a panel on the right side where you can see the notes and the media info.

.. figure:: /_static/images/Preview_Sidebar.jpg
   :alt: Preview Sidebar

   Preview Sidebar


View the comments
^^^^^^^^^^^^^^^^^
*Under* ``Notes`` *tab*

When a comment indicates a frame or a range of frames, it means the comment is related to the frame or the specified range.
Clicking on the text will move the playhead to the first frame of the text annotation. ``Shift + Click`` or clicking on the chronometer icon will also set the preview range to allow you to loop into this range.

.. figure:: ../images/Comment_Annotation.png
   :alt: Text annotation example

   Text annotation example


.. video:: ../images/Comment_See.mp4
    :autoplay: yes
    :loop: yes
    :controls: no

See also `how to write comments`_.

.. _`how to write comments`: :ref:`comment`


View the media info
^^^^^^^^^^^^^^^^^^^^
*Under* ``Info`` *tab*

If the media has a ``stax_media_info`` custom property, you can see media related information.

.. figure:: /_static/images/MediaInfo_Preview.jpg
   :alt: Media information in preview

   Media information in preview

.. seealso::

   How to add these information: :ref:`media-info`

Change displayed track
----------------------

One other feature is to **choose the track you want to display**.

You can **change** displayed track by using the **displayed track dropdown list**.

.. figure:: ../images/ChangeDisplayedTrack.png
   :alt: Change displayed track

   Change displayed track

Shortcuts
^^^^^^^^^^
* Using the ``Ctrl + Arrow Up/Down`` to change the current displayed track to the one above/below. When reaching highest/lowest tracks, it'll loop from bottom/top.
* Using the ``NUMPAD #`` keys from 1 to 9 will select the tracks by their index.

Playback range
--------------

If you want to watch a specific part of the timeline, you can set a ``Preview Range``. 
The animation will loop play between these two frames.

Box
^^^
Using a box selection with the mouse.

.. video:: ../images/SetPreviewRange_Box.mp4
    :autoplay: yes
    :loop: yes
    :controls: no

Selected
^^^^^^^^
Using ``Shift + Left click`` to select sequences. The currently displayed sequence is selected by default.

.. video:: ../images/SetPreviewRange_Selected.mp4
    :autoplay: yes
    :loop: yes
    :controls: no

Compare
=======
Stax provides features to compare the medias.

.. _`toggle meta`:

Toggle meta
-----------
If you are displaying a meta sequence with nested media sequences, you can display the inside stack in the timeline by clicking on ``Toggle Meta``.

.. figure:: /_static/images/ToggleMeta_Button-Open.jpg
    :alt: Toggle Meta Button

    Toggle Meta Button


Overlay
-------

All overlay comparison tools are accessible from the top right corner of the *Preview*.
The current displayed media is compared with the first media under.

.. important:: Media affected by comparison are only the ones in the currently displayed track.

Alpha opacity (Default)
^^^^^^^^^^^^^^^^^^^^^^^

.. figure:: ../images/Compare_Alpha.jpg
   :alt: Compare Default

   Compare Default

Difference
^^^^^^^^^^

.. figure:: ../images/Compare_Difference.jpg
   :alt: Compare Difference

   Compare Difference

Multiply
^^^^^^^^

.. figure:: ../images/Compare_Multiply.jpg
   :alt: Compare Multiply

   Compare Multiply

Wipe
^^^^

.. figure:: ../images/Compare_Wipe.jpg
   :alt: Compare Wipe

   Compare Wipe

* Split: Move the split position.
* Angle: Change the split's orientation.
* Blur: Smoothes the split's edge.

.. warning:: Not using ``Default`` blending while playing allows you to compare medias but can lead to performances loss.


Side by side
------------

Click on the ``Side by side`` button to get a side by side display.
Choose two different tracks to see both of them at the same time.

.. video:: ../images/Compare_SideBySide.mp4
   :autoplay: yes
   :loop: yes
   :controls: no

   Animation and Rendering at the same time


Detach view
-----------

Click on the **Detach view** button to get a new floating Preview window.
Choose two different tracks between the main Preview and the new Preview window to see both of them at the same time.


.. video:: ../images/Compare_DetachView.mp4
   :autoplay: yes
   :loop: yes
   :controls: no

   Detach Preview in new window

.. _`edit`:

Edit
====

.. important::
   Stax uses the concept of review session.  
   You can annotate and comment every medias on your timeline during a review session.  
   Your changes will take effect only after you decide to `Publish`_. 
   You can save your work with ``Ctrl + S``. If you close Stax or Blender crashes during a review session, you'll be allowed to retrieve your session as it was.
   To clear your modifications, you can `Abort`_.
   Until you exit the review session by aborting or publishing, you cannot update the session and the `Update` button is disabled.
 

.. _reviewed:

What did I review?
------------------
Reviewed media in the current session are indicated by a ``*`` over the sequence in the timeline and before the media name in the ``Preview``.


.. list-table:: 

    * - .. figure:: ../images/Reviewed_Media-name.png
           :align: center

           "Reviewed" Media Name

      - .. figure:: ../images/Reviewed_Sequence.png
           :align: center

           "Reviewed" Sequence


Change status
-------------
A dropdown list allows you to choose the new reviewing status you want to adress to the media.

.. figure:: ../images/StatusDropdown.png
   :alt: Status selection

   Status selection


Draw
----
You can directly draw in the *Preview* area with the ``Annotation`` tool selected by **left clicking**.

**Right click** for the eraser.

See `how to hide/show annotations`_.

.. _`how to hide/show annotations`: `View the annotations`_

.. figure:: ../images/Annotate.png
   :alt: Annotate tool

   Annotate tool

By holding a left click on the tool, in the toolbar, you can change your drawing tool.

.. tip:: 
   Stax supports Drawing Tablets.

In the lower bar you can change the current color, create new layers and hide the drawings.

.. note::
   The color is set for the whole layer with this tool.


Onion skin
^^^^^^^^^^

The annotate tool has also a simple Onion Skin system. In the right sidebar, under ``View`` tab.

.. figure:: /_static/images/Preview_Onion-Skin.jpg
   :alt: Annotate tool Onion Skin

   Annotate tool Onion Skin


.. tip::
   If you need more drawing tools, see :doc:`./reviewing_advanced_drawing`.

External Software
-----------------

If you have a software you love and you don't find shoe to your foot with Stax's tools, you can use an external image editor. See :ref:`set-external-image-editor`.

-  **Click** on the **Edit in …** button (*Photoshop*, in the
   example)
-  **Wait** for your editor to launch with the loaded picture
-  **Do** your edition
-  **Save** the picture
-  **Close** the editor
-  **Left+click** anywhere in Blender **to refresh** the view
   and see your externally edited annotation.

.. video:: ../images/ExternalEdit.mp4
   :autoplay: yes
   :loop: yes
   :controls: no

   Externally edited annotation launch

.. note::

   Externally edited annotations always have a duration of one frame.

.. _pending-annotations-drawings:

Pending annotation drawings
---------------------------
Pending annotations drawings are indicated with markers similar to actual `image annotations`_ although they are taller and have a different color.
Once the notes are published, the pending annotations drawings will be displayed as the actual ones.

.. figure:: /_static/images/ImageAnnotation_SequencerMarker_Pending.png
   :alt: Pending image annotations drawings

   Pending image annotations drawings

.. _image annotations: `View the annotations`_

Clear drawings
--------------
To clear any drawing made over a media, press `Clear Drawings` when the media is displayed.


.. figure:: ../images/ClearDrawings.jpg
   :alt: Clear Drawings Button

   Clear Drawings Button


.. include:: edit_comment.rst


.. _publish:

Publish
=======
As you're working into a ==review session==, nothing is changed or sent out of Stax until you click on ``Publish Reviews``.

Publishing the reviews will automatically update or create the reviews in the linked directory by adding all the notes created during the session (see :ref:`edit`).

.. note::
   If you haven't linked any reviews directory before, a file browser will open and ask you to select a directory to export the reviews. This allows you to share them later.

.. figure:: /_static/images/PublishReviews_Button.jpg
   :alt: Publish Button

   Publish Button

Abort
=====
If you want to start over your review session, you can reset your changes by pressing ``Abort Reviews``.

.. warning:: All of your annotations, drawings and comments will be deleted!

.. figure:: /_static/images/AbortReviews_Button.jpg
   :alt: Abort Button

   Abort Button
