Override Classes
================
Stax's code is fully overridable. You can customize **every** part of Stax by adding, modifying or removing things.


What's the point?
-----------------

Using overrides allows you to use existing Stax functions/operators in the middle of your own code.
This way, it is simple to add processes before and after an operation, or to completely change a behaviour. 

This makes maintainability easier, you do not have to copy/paste blocks of code in your own script, losing at the same time improvements that could be made by Stax's team.
Also, this gives a full control over the UI by allowing the possibility to insert your own buttons or change the UI's feedback depending on your needs.

Here are some useful Blender Python API links:

* Panel_
* Menu_
* Header_ 
* Operator_
* `Property Definitions`_
* PropertyGroup_

.. _Panel: https://docs.blender.org/api/current/bpy.types.Panel.html
.. _Menu: https://docs.blender.org/api/current/bpy.types.Menu.html
.. _Header: https://docs.blender.org/api/current/bpy.types.Header.html
.. _Operator: https://docs.blender.org/api/current/bpy.types.Operator.html
.. _`Property Definitions`: https://docs.blender.org/api/current/bpy.props.html
.. _PropertyGroup: https://docs.blender.org/api/current/bpy.types.PropertyGroup.html


Boilerplates
------------

Authentication
^^^^^^^^^^^^^^^
:meth:`stax.ops.ops_session.WM_OT_ProductionManagerAuthentication`.

.. code-block:: python

   class WM_OT_ProductionManagerAuthentication(bpy.types.Operator):
      """Log-in to synchronize Stax with Production Manager"""  # Put custom description, will be displayed as a tooltip

      bl_idname = "wm.pm_authentication"  # /!\ Do not rename, it makes the override possible
      bl_label = "Production Manager authentication"  # Put a custom name or keep this one

      login: bpy.props.StringProperty(name="User")
      password: bpy.props.StringProperty(name="Password", subtype="PASSWORD")
      save_credentials: bpy.props.BoolProperty(
         name="Save Credentials", update=switch_save_credentials
      )
      auto_login: bpy.props.BoolProperty(name="Auto Log In", update=switch_auto_login)

      def invoke(self, context, _event):  # Run when called through UI (click or shortcut)
         # Open dialog with auto attributes from operator's properties
         return context.window_manager.invoke_props_dialog(self)

      def execute(self, context):  # Run when clicking on "OK" in the dialog
         # ---
         # Your authentication code 
         # ---

         return {'FINISHED'}


Publish Reviews
^^^^^^^^^^^^^^^
:meth:`stax.ops.ops_session.STAX_OT_publish_reviews`.

.. code-block:: python

   class STAX_OT_publish_reviews(bpy.types.Operator):
      """Publish reviews of the current session"""  # Put custom description, will be displayed as a tooltip

      bl_idname = "sequencer.publish_reviews"  # /!\ Do not rename, it makes the override possible
      bl_label = "Publish Reviews"  # Put a custom name or keep this one

      filter_glob: StringProperty(  # Match any file againt glob, must remain like that
         default="",
         options={"HIDDEN"},
      )

      filename_ext = ""  # Match any extension, must remain like that

      directory: bpy.props.StringProperty(
         name="Outdir Path",
         description="Where I will save my stuff",
         subtype="DIR_PATH",
      )

      @classmethod
      def poll(cls, context):
         if context.scene.review_session_active:
               return True

      def invoke(self, context, _event):  # Run when called through UI (click or shortcut)
         # Run the original invoke
         return stax.ops.ops_session.STAX_OT_publish_reviews.invoke(self, context, event)

      def execute(self, context):
         # ---
         # Your code as a pre-publish
         # ---

         # Run the original execute
         result = stax.ops.ops_session.STAX_OT_publish_reviews.execute(self, context)

         # ---
         # Your code as a post-publish
         # ---

         return result