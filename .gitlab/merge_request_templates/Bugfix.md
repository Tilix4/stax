## Description of fix



## How to test it



## Checklist

Please check if your MR fulfills the following requirements:
- [ ] Build was run locally and any changes were pushed
- [ ] Lint has passed locally without error
- [ ] CHANGELOG.md has been updated
- [ ] Branch has been rebased on merge branch


**FYI**: @awesome_dev_too

/label ~Bug

