# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

"""
Every ui class (panel, menu or header) relative to the general Stax's UI
"""
from pathlib import Path

import bpy
from bpy.types import Header, Menu
from bpy.utils import register_class, unregister_class

import stax


class TOPBAR_HT_upper_bar(Header):
    """Top bar space header"""

    bl_space_type = "TOPBAR"

    def draw(self, context):
        layout = self.layout

        layout.separator()
        if context.region.alignment == "TOP":
            # "Stax" button to click on to display information
            box = layout.box()
            box.menu("TOPBAR_MT_stax")

            row = layout.row()
            # Session menu
            row.menu("TOPBAR_MT_session")

            # Workspaces
            row.menu("TOPBAR_MT_workspaces")

            # Disable when advanced drawing
            row.enabled = (
                bpy.context.workspace is not bpy.data.workspaces["Advanced drawing"]
            )

            layout.separator_spacer()

            # Abort/Publish Reviews button
            if context.scene.review_session_active:
                layout.separator()
                row = layout.row()
                row.operator("scene.abort_reviews", icon="CANCEL")
                row.operator("scene.publish_reviews", icon="CHECKMARK")

            layout.separator_spacer()

            # Reports banner for messages to the user
            layout.template_reports_banner()

        else:
            pass


class TOPBAR_MT_stax(Menu):
    """Stax menu"""

    bl_label = "Stax"

    def draw(self, context):
        scene = context.scene
        layout = self.layout

        # Stax version
        layout.label(text=f"Version: {stax.version}")

        layout.separator()

        # Current loaded timeline
        if scene.timeline:
            layout.label(text=f"Timeline: {scene.timeline}")

        # Current linked reviews
        reviews_linked_dir = scene.reviews_linked_directory
        if reviews_linked_dir:
            layout.label(text=f"Reviews: {reviews_linked_dir}")

        layout.separator()

        # Help
        layout.label(text="Help")
        col = layout.column()
        col.operator(
            "wm.open_weburl", text="Documentation"
        ).url = "https://superprod.gitlab.io/stax_suite/stax/docs/"
        col.operator(
            "wm.open_weburl", text="Support"
        ).url = "https://discord.gg/8ZNx7Vz"
        col.operator(
            "wm.open_weburl", text="Repository"
        ).url = "https://gitlab.com/superprod/stax"

        # TODO Button to open an issue with information filled like version, system...


class TOPBAR_MT_session(Menu):
    """All Session operations"""

    bl_label = "Session"

    def draw(self, context):
        layout = self.layout

        layout.operator_context = "INVOKE_AREA"
        layout.operator(
            "wm.read_homefile", text="New", icon="FILE_NEW"
        ).app_template = context.preferences.app_template
        layout.operator("wm.open_mainfile", text="Open...", icon="FILE_FOLDER")
        layout.menu("TOPBAR_MT_file_open_recent")
        layout.operator("wm.revert_mainfile")
        layout.menu("TOPBAR_MT_file_recover")

        layout.separator()

        layout.operator_context = (
            "EXEC_AREA" if context.blend_data.is_saved else "INVOKE_AREA"
        )
        layout.operator("wm.save_mainfile", text="Save", icon="FILE_TICK")

        layout.operator_context = "INVOKE_AREA"
        layout.operator("wm.save_as_mainfile", text="Save As...")
        layout.operator("wm.save_as_mainfile", text="Save Copy...").copy = True

        layout.separator()

        # Import editing
        if [
            a.module
            for a in context.preferences.addons
            if "vse" in a.module and "io" in a.module
        ]:
            layout.operator("sequencer.import_editing", icon="ASSET_MANAGER")

        layout.separator()

        # Update
        layout.operator("sequencer.update_sequences_reviews", icon="FILE_REFRESH")

        # Reviews menu
        layout.menu("TOPBAR_MT_session_reviews", icon="DOCUMENTS")

        layout.separator()

        layout.operator("wm.quit_blender", text="Quit", icon="QUIT")


class TOPBAR_MT_session_reviews(Menu):
    """Session Reviews menu"""

    bl_label = "Reviews"

    def draw(self, context):
        scene = context.scene
        layout = self.layout

        # (Un)link reviews operator, displayed if sequences
        if scene.timeline or scene.sequence_editor.sequences:
            col = layout.column()
            col.enabled = not context.scene.review_session_active

            # Link
            link_type = context.scene.reviews_properties.link_type
            ops = col.operator(
                "sequencer.link_sequences_reviews", text="Link Selected Sequences"
            )
            ops.action = "LINK"
            ops.link_type = link_type

            ops = col.operator(
                "sequencer.link_sequences_reviews", text="Link All Sequences"
            )
            ops.action = "LINK"
            ops.link_type = link_type
            ops.all = True

            # Unlink
            col.operator(
                "sequencer.link_sequences_reviews", text="Unlink Sequences Reviews"
            ).action = "UNLINK"

        layout.separator()

        # Publish/Abort
        layout.operator("scene.abort_reviews", icon="CANCEL")
        layout.operator("scene.publish_reviews", icon="CHECKMARK")


class TOPBAR_MT_workspaces(Menu):
    """Workspace Selection"""

    bl_label = "Workspace"

    def draw(self, _):
        layout = self.layout

        layout.operator_context = "INVOKE_AREA"

        # Main
        layout.operator(
            "wm.change_workspace", icon="IMAGE_BACKGROUND", text="Play and Review"
        ).workspace = "Main"

        # Export editing
        layout.operator(
            "wm.change_workspace", icon="EXPORT", text="Export timeline"
        ).workspace = "Timeline export"

        # Settings
        layout.operator(
            "wm.change_workspace", icon="PREFERENCES", text="Configuration"
        ).workspace = "Stax configuration"


classes = [
    TOPBAR_HT_upper_bar,
    TOPBAR_MT_stax,
    TOPBAR_MT_session,
    TOPBAR_MT_session_reviews,
    TOPBAR_MT_workspaces,
]


def register():
    # Set Custom UI
    for cls in classes:
        register_class(cls)


def unregister():
    # Clear Custom UI
    for cls in classes:
        unregister_class(cls)
