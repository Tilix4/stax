# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
import bpy
from bpy.types import Panel

from stax.utils.utils_reviews import get_active_pending_note_sequence
from stax.utils.utils_timeline import (
    get_media_sequence,
    get_review_sequence,
    get_sequence_channel_frame,
)
from stax.utils.utils_ui import (
    display_note,
    get_stax_icon,
)


class NotesPanel(Panel):
    """Main Notes panel"""

    bl_region_type = "UI"
    bl_category = "Notes"

    @staticmethod
    def has_notes(context):
        space_data = context.space_data
        return space_data.view_type == "PREVIEW" or space_data.type == "VIEW_3D"

    @classmethod
    def poll(cls, context):
        return (
            cls.has_notes(context)
            and bpy.data.scenes["Scene"].sequence_editor.active_strip
        )

    def draw_header(self, context):
        layout = self.layout

        layout.operator(
            "wm.write_comment",
            icon="ADD",
            text="",
        )


class NOTES_PT_pending_text_note(NotesPanel):
    """Pending text note panel"""

    bl_label = "Pending note"

    def draw(self, context):
        layout = self.layout
        scene = bpy.data.scenes["Scene"]
        media_sequence = get_media_sequence(scene.sequence_editor.active_strip)
        pending_note_sequence = get_active_pending_note_sequence(media_sequence)

        tool_shelf = None
        area = context.area

        for region in area.regions:
            if region.type == "UI":
                tool_shelf = region

        width = tool_shelf.width / 8

        # Display pending note
        if pending_note_sequence:
            display_note(layout, width, media_sequence, pending_note_sequence)

        # Write new text comment
        write_comment_icon = get_stax_icon("write_comment")
        layout.operator(
            "wm.write_comment",
            icon_value=write_comment_icon.icon_id,
            text="New comment",
        )


class NOTES_PT_text_notes(NotesPanel):
    """All current media text notes panel"""

    bl_label = "Text notes"

    def draw_header(self, context):
        layout = self.layout
        wm = context.window_manager

        # Add comment button
        NOTES_PT_pending_text_note.draw_header(self, context)

        layout.prop(
            wm,
            "newer_to_older",
            icon="TRIA_UP" if wm.newer_to_older else "TRIA_DOWN",
            text="",
        )

    def draw(self, context):
        layout = self.layout
        wm = context.window_manager
        space_data = context.space_data
        scene = bpy.data.scenes["Scene"]
        seq_ed = scene.sequence_editor

        # Get media sequence of displayed channel or active strip as default
        displayed_sequence = (
            get_sequence_channel_frame(
                seq_ed.sequences, space_data.display_channel, scene.frame_current
            )
            or seq_ed.active_strip
        )
        media_sequence = get_media_sequence(displayed_sequence)

        # Abort if no media sequence
        if not media_sequence:
            return

        review_sequence = get_review_sequence(media_sequence)

        # TODO check if it's possible to make it common with other occurrences
        tool_shelf = None
        area = context.area

        for region in area.regions:
            if region.type == "UI":
                tool_shelf = region

        width = tool_shelf.width / 8

        # Sentinel to not try to display comments is there is no any review associated to the media
        if review_sequence:

            # Sort notes from their replies
            replies = {}  # {parent_parent_name: [reply1_name, reply2_name...]}
            notes = []
            for seq in review_sequence.sequences:
                parent = seq.get("parent")
                if parent:
                    replies.setdefault(parent, [])
                    replies[parent].append(seq.name)
                else:
                    notes.append(seq)

            notes_to_display = reversed(notes) if wm.newer_to_older else notes
            for stax_note in notes_to_display:
                contents = stax_note.get("contents", {})

                # Sentinel to avoid displaying textless notes or pending notes
                if (
                    not contents.get("TextComment")
                    and not contents.get("TextAnnotation")
                    and not stax_note.sequences
                ) or stax_note.name.startswith("pending."):
                    continue

                box = (
                    display_note(layout, width, media_sequence, stax_note)
                    or layout.box()
                )

                box.separator_spacer()

                # Replies
                col = box.row()
                split = col.split(factor=0.05)
                left = split.column()

                right = split.column()

                for reply_name in replies.get(stax_note.name, []):
                    # Get the object by the reference's name
                    reply = review_sequence.sequences.get(reply_name)

                    display_note(right, width / 1.05, media_sequence, reply)

                # Reply Button
                if not stax_note.name.startswith("pending."):
                    footer = box.row()
                    ops = footer.operator(
                        "wm.write_comment",
                        text="New Reply",
                    )
                    ops.parent_name = stax_note.name
