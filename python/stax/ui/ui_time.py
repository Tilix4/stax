# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

import bpy
from bpy.types import Header, UILayout


class TIME_HT_editor_buttons(Header):
    bl_space_type = "DOPESHEET_EDITOR"
    bl_label = ""

    def draw(self, context):
        pass

    @staticmethod
    def draw_author(layout: UILayout):
        """Author Name UI.

        :param layout: Layout to draw the author name in
        """
        user_preferences = bpy.context.scene.user_preferences
        layout.prop(user_preferences, "author_name")

    def draw_header(self, context, layout):
        scene = context.scene
        screen = context.screen

        # User settings
        row = layout.row(align=True)
        row.operator(
            "wm.change_workspace", text="", icon="PREFERENCES"
        ).workspace = "Stax configuration"

        # Author name
        self.draw_author(row)

        layout.separator_spacer()

        # Playback buttons
        row = layout.row(align=True)
        row.operator("screen.frame_jump", text="", icon="REW").end = False
        row.operator("screen.keyframe_jump", text="", icon="PREV_KEYFRAME").next = False
        if not screen.is_animation_playing:
            # if using JACK and A/V sync:
            #   hide the play-reversed button
            #   since JACK transport doesn't support reversed playback
            if (
                scene.sync_mode == "AUDIO_SYNC"
                and context.preferences.system.audio_device == "JACK"
            ):
                row.scale_x = 2
                row.operator("screen.animation_play", text="", icon="PLAY")
                row.scale_x = 1
            else:
                row.operator(
                    "screen.animation_play", text="", icon="PLAY_REVERSE"
                ).reverse = True
                row.operator("screen.animation_play", text="", icon="PLAY")
        else:
            row.scale_x = 2
            row.operator("screen.animation_play", text="", icon="PAUSE")
            row.scale_x = 1
        row.operator("screen.keyframe_jump", text="", icon="NEXT_KEYFRAME").next = True
        row.operator("screen.frame_jump", text="", icon="FF").end = True

        layout.separator_spacer()

        # Current frame
        row = layout.row(align=True)
        row.scale_x = 0.9
        row.prop(scene, "frame_current", text="")
        if scene.sequence_editor.active_strip:
            row.prop(scene, "media_current_frame", text="Media")

        # Start and End frame
        row = layout.row(align=True)
        row.scale_x = 0.8
        row.prop(scene, "frame_start", text="Start")
        row.prop(scene, "frame_end", text="End")


classes = [TIME_HT_editor_buttons]


def register():
    for cls in classes:
        bpy.utils.register_class(cls)


def unregister():
    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)
