# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
"""
Every utils function related to timeline management
"""
from pathlib import Path
from typing import Dict, List, Tuple, Union

import bpy
from bpy.types import (
    Collection,
    ImageSequence,
    MetaSequence,
    MovieSequence,
    Sequence,
    SequenceEditor,
    WipeSequence,
)

from .utils_core import get_context

MediaSequence = Union[ImageSequence, MovieSequence]


def get_current_sequence(channel=0):
    """Get the current displayed sequence

    TODO: Will be optimized with: https://developer.blender.org/D7679

    :param channel: Override channel to get current sequence. Useful when two PREVIEW windows.
    """
    scene = bpy.data.scenes["Scene"]
    sequences_stack = (
        scene.sequence_editor.meta_stack[-1].sequences
        if scene.sequence_editor.meta_stack
        else scene.sequence_editor.sequences
    )

    # Return the clip matching the displayed channel or the visible one
    # --------
    current_space = get_context("Main", "PREVIEW").get("space_data")
    displayed_track_channel = channel or current_space.display_channel

    # Get sequences under playhead, optimize it with https://developer.blender.org/D7679 when it lands
    under_playhead_sequences = get_sequences_under_playhead(sequences_stack)

    if under_playhead_sequences:
        # Check if it's global display, take the top sequence
        if displayed_track_channel == 0:
            return under_playhead_sequences[-1]

        # Get the current displayed sequence
        for seq in reversed(under_playhead_sequences):
            if seq.channel <= displayed_track_channel:
                return seq
    else:
        return


def get_media_sequence(input_sequence: Sequence) -> Union[MovieSequence, ImageSequence]:
    """Return the media_sequence for a given input_sequence.

    The media sequence is the sequence of the currently displayed media, and the highest one in the tracks when blending.
    It can only be an image or a movie.
    Useful when the input_sequence is a MetaSequence, return the main displayed media sequence by iterating in sub-sequences.
    If input_sequence has `media_sequence_name` set, try to return the sequence with this name.

    :param input_sequence: Sequence to get the media sequence from
    :return: media sequence (image or movie media sequence)
    """
    scene = bpy.context.scene

    # Sentinel
    if not input_sequence:
        return
    elif input_sequence.type in ["IMAGE", "MOVIE"]:
        return input_sequence

    # Get media sequence
    sequence_editor = bpy.data.scenes["Scene"].sequence_editor
    all_sequences = sequence_editor.sequences_all
    media_sequence_name = input_sequence.get("media_sequence_name", "")

    if media_sequence_name:
        return all_sequences.get(media_sequence_name)
    else:  # Fall back on first Movie or Image sequence found
        # Return input sequence if not a meta and the playhead is over it
        if (
            input_sequence.type in ["IMAGE", "MOVIE"]
            and input_sequence.frame_final_start
            <= scene.frame_current
            < input_sequence.frame_final_end
        ):
            return input_sequence
        # If meta, iterate in hierarchy to find the current one from the highest seq to the bottom
        elif input_sequence.type == "META":
            for seq in reversed(input_sequence.sequences):
                media_sequence = get_media_sequence(seq)
                if media_sequence:
                    return media_sequence


def get_review_sequence(media_sequence: MediaSequence) -> MetaSequence:
    """Get review sequence of media sequence.

    :param media_sequence: Media sequence to get review sequence
    :return: Review sequence
    """
    review_sequence_name = media_sequence.get("review_sequence_name")
    if review_sequence_name:
        return bpy.data.scenes["Scene"].sequence_editor.sequences_all.get(
            review_sequence_name
        )


def get_visible_sequences() -> List[MovieSequence]:
    """Return a list of all the visible sequences.

    .. code-block::

        [=]: selected sequence, [x]: not selected sequence, ->: active track
        -------------------------------
        |     [xxxxxxx]                |
        |->  [===][=][===]               |
        |   [====][xxx]  [====]        |
        |  [====][xxxxx]        [====] |
        --------------------------------

    :return: List of visible sequences"""
    scene = bpy.context.scene

    # Ensure current sequence
    scene.frame_set(scene.frame_current)
    current_sequence = scene.sequence_editor.active_strip

    # Sentinel
    if not current_sequence:
        return set()

    # Try cached data
    cached_visible_sequences = scene.get("visible_sequences")
    if cached_visible_sequences:
        return frozenset(
            {
                scene.sequence_editor.sequences.get(name)
                for name in cached_visible_sequences
            }
        )

    # Iterate all sequences
    visible_sequences = set()
    filled_ranges = set()  # Keep filled ranges for optim
    active_channel = current_sequence.channel
    for sequence in reversed(scene.sequence_editor.sequences):
        # Match all sequences with current channel or below
        if sequence.channel == active_channel:
            visible_sequences.add(sequence)
            filled_ranges.add((sequence.frame_final_start, sequence.frame_final_end))
        elif sequence.channel < active_channel:
            # Get the current displayed sequence
            for frame in range(scene.frame_start, scene.frame_end):
                # Check if range is already filled by a sequence
                # ---------
                range_filled = False
                for filled_range in filled_ranges:
                    if filled_range[0] <= frame < filled_range[1]:
                        range_filled = True
                        break

                if range_filled:
                    continue
                # ---------

                # Check if frame is between sequence's boundaries
                if sequence.frame_final_start <= frame < sequence.frame_final_end:
                    visible_sequences.add(sequence)
                    filled_ranges.add(
                        (sequence.frame_final_start, sequence.frame_final_end)
                    )

                    # Continue to next sequence
                    continue

    # Set active sequence back
    if current_sequence:
        bpy.ops.sequencer.select_all(action="DESELECT")
        current_sequence.select = True
        scene.sequence_editor.active_strip = current_sequence

    return frozenset(visible_sequences)


def get_sequences_under_playhead(
    sequences,
) -> List[Sequence]:
    """Get sequences under playhead.

    TODO optimize it with https://developer.blender.org/D7679 when it lands

    :param sequences: List of sequences where to look into
    :return: Sequences currently under playhead
    """
    scene = bpy.context.scene

    under_playhead_sequences = [
        seq
        for seq in sequences
        if (
            seq.frame_final_start <= scene.frame_current
            and seq.frame_final_end > scene.frame_current
        )
    ]

    return under_playhead_sequences


def get_sequence_channel_frame(sequences, channel: int, frame: int) -> Sequence:
    """Get sequence at channel and frame.

    :param sequences: List of sequences where to look into
    :param channel: Channel to query sequence in
    :param frame: Frame to query sequende under
    :return: Matched sequence
    """
    channel_sequences = [seq for seq in sequences if seq.channel == channel]
    for seq in channel_sequences:
        if seq.frame_final_start <= frame and seq.frame_final_end > frame:
            return seq


def get_available_tracks() -> Dict[int, str]:
    """Get available tracks based on different cases.

    Return all channels from the first one to the last channel having a sequence.
    Empty ones are displayed to avoid index mismatch due to EnumProperty.

    :return: Channels: Available tracks names
    """
    scene = bpy.context.scene
    meta_stack = scene.sequence_editor.meta_stack

    def build_items_list(sequences):
        # Use sequences under playhead
        under_playhead_seqs = get_sequences_under_playhead(sequences)

        # Build items list
        available_tracks = {}
        sequences_index = 0
        visible_seqs_count = len(under_playhead_seqs)
        # NB: Range start and end cannot be changed: https://developer.blender.org/T42194
        for i in range(1, 32):
            # Is there any sequences remaining
            if visible_seqs_count <= sequences_index:
                break
            remaining_sequences = under_playhead_seqs[sequences_index:]

            # Match sequence
            sequence = None
            for seq in remaining_sequences:
                if seq.channel == i:
                    sequence = seq
                    sequences_index += 1
                    break

            # Append item
            if sequence:
                available_tracks.update({i: seq.name})
            else:
                available_tracks.update({i: f"Channel {i}"})

        return available_tracks

    if meta_stack:  # Create list from current sequences under playhead
        current_meta = meta_stack[-1]

        available_tracks = build_items_list(current_meta.sequences)

    else:
        if hasattr(scene, "tracks") and scene.tracks:  # Create list from tracks
            available_tracks = {i: seq.name for i, seq in enumerate(scene.tracks, 1)}

        else:  # Create list from sequences
            available_tracks = build_items_list(scene.sequence_editor.sequences)

    # Add global display
    available_tracks.update({0: "All"})

    return available_tracks


def get_reviewed_media_sequences() -> List[MediaSequence]:
    """Get reviewed media sequences from current scene editing.

    :return: Reviewed media sequences
    """
    return [
        seq
        for seq in bpy.context.scene.sequence_editor.sequences_all
        if seq.get("reviewed") and not seq.lock
    ]


def get_reviewable_media_sequences() -> List[MediaSequence]:
    """Get reviewable media sequences from current scene editing.

    :return: Reviewable media sequences
    """
    return [
        seq
        for seq in bpy.context.scene.sequence_editor.sequences_all
        if seq.type in ["IMAGE", "MOVIE"] and not seq.get("stax_kind")
    ]


def get_source_media_path(sequence: Sequence) -> Path:
    """Return source media path of a given sequence.

    :param sequence: Sequence to get source media path
    :return: Path of source media
    """
    if sequence.type == "IMAGE":
        return Path(bpy.path.abspath(sequence.directory), sequence.elements[0].filename)
    elif sequence.type == "SOUND":
        return Path(bpy.path.abspath(sequence.sound.filepath))
    else:
        return Path(bpy.path.abspath(sequence.filepath))


def get_metas_hierarchy(sequence: Sequence) -> List[MetaSequence]:
    """Return all meta sequences hierarchy of the given sequence.

    Always start with the holding sequence_editor.

    :param sequence: Sequence to get meta sequences hierarchy
    :return: List of all meta sequences from the main view to the most nested one
    """
    sequence_editor = bpy.data.scenes["Scene"].sequence_editor

    # Sentinel for sequence into main timeline
    if sequence_editor.sequences.get(sequence.name):
        return [sequence_editor]

    # Match sequence
    all_metas = []
    parent_stack = get_holding_stack(sequence)
    while parent_stack is not None:
        all_metas.insert(0, parent_stack)
        parent_stack = get_holding_stack(parent_stack)

    return all_metas


def get_holding_stack(sequence: Sequence) -> Union[MetaSequence, Collection]:
    """Return sequences stack holding the given sequence.

    # TODO implement into a get_parent() in Blender core

    :param sequence: Sequence to get sequences stack
    :return: List of all meta sequences from the main view to the most nested one
    """
    sequence_editor = bpy.data.scenes[
        "Scene"
    ].sequence_editor  # TODO make it more versatile with bpy.context.scene

    if not hasattr(sequence, "name"):  # Sentinel for sequence_editor
        return

    # Sentinel for sequence into main timeline
    if sequence_editor.sequences.get(sequence.name):
        return sequence_editor

    # Match sequence
    for seq in sequence_editor.sequences_all:
        if seq.type == "META":
            if sequence.name in seq.sequences:
                return seq


def get_wipe_sequence(
    sequence: Union[MovieSequence, MetaSequence, ImageSequence]
) -> WipeSequence:
    """Get wipe sequence of given sequence.

    :param sequence: Sequence to get wipe sequence from
    :return: Wipe sequence of given sequence
    """
    if not sequence:  # Sentinel
        return

    return bpy.context.scene.sequence_editor.sequences_all.get(sequence.get("wipe", ""))


def update_preview_channel():
    """Update track displayed in the preview by changing the channel."""
    context = bpy.context
    scene = context.scene
    screen = context.screen

    if screen:
        #   Set the correct display_channel to the space data.
        # If it is run from the preview space, then update the channel (choose track to display enum list)
        # Else, iterate in every area of the screen and when first PREVIEW is encountered, change its channel
        if context.space_data:
            current_space = None
            if (
                hasattr(context.space_data, "view_type")
                and context.space_data.view_type == "PREVIEW"
            ):
                current_space = context.space_data
            else:
                for area in reversed(screen.areas):
                    space = area.spaces[0]
                    if hasattr(space, "view_type") and space.view_type == "PREVIEW":
                        current_space = space
                        break
                    else:
                        current_space = context.space_data
        else:
            current_space = get_context("Main", "PREVIEW").get("space_data")

        current_space.display_channel = int(scene.current_track)

        # Inform the user about the picked track
        available_tracks = get_available_tracks()
        track_name = available_tracks[current_space.display_channel]
        bpy.ops.wm.report_message(
            type="DEBUG",
            message=f"Channel {current_space.display_channel} as '{track_name}' displayed",
        )

        # Update the active strip
        scene.frame_set(scene.frame_current)

    # Update comparison
    compare_properties = scene.compare_properties
    if compare_properties.compare_type != "CROSS":
        compare_properties.compare_type = compare_properties.compare_type


def display_media_annotations(media_sequence: Sequence, display_state: bool) -> bool:
    """Set the display state of media annotations

    :param sequence: Media sequence with annotations
    :param display: Display state to set to the annotations
    :return: Boolean if any annotations have been displayed
    """
    any_annotation_displayed = False

    metas_hierarchy = get_metas_hierarchy(media_sequence)
    if metas_hierarchy:
        # Get direct parent meta sequence where annotations sequences are stored
        parent_meta = metas_hierarchy[-1]
        for seq in parent_meta.sequences:
            if seq.get("stax_kind") in {
                "note",
                "annotation",
                "temp",
                "externally_edited",
            }:
                seq.mute = not display_state
                any_annotation_displayed = True

    return any_annotation_displayed


def build_substitute(
    stack: MetaSequence, media_sequence: MediaSequence
) -> MetaSequence:
    """Build substitute of given media sequence under the given stack.

    :param stack: Stack holding the substitute
    :param media_sequences: Media sequence to build substitute of
    :return: Built meta sequence substitute
    """
    # Keep source stack duration if already a meta
    stack_duration = (
        stack.frame_final_duration
        if hasattr(stack, "type")
        else media_sequence.frame_final_duration
    )

    # Create meta substitute
    meta = stack.sequences.new_meta(
        f"stax.{media_sequence.name}",
        media_sequence.channel,
        media_sequence.frame_final_start,
    )
    meta["stax_kind"] = "substitute"

    # Move sequence in meta
    media_sequence.move_to_meta(meta)

    # Update and force duration if meta TODO see for a move_to_meta(..., update_meta=False) to avoid this unexpected behaviour
    if hasattr(stack, "type"):
        stack.update()
        if stack.frame_final_duration != stack_duration:
            stack.frame_final_duration = stack_duration
            stack.update()

    return meta


def set_preview_range_from_sequences(
    sequences: List[Union[MetaSequence, MovieSequence]]
):
    """Set timeline preview range to sequences boundaries

    :param sequences: Sequences to set the preview range from
    """
    scene = bpy.context.scene

    # Get boundaries
    start = scene.frame_end
    end = scene.frame_start
    for seq in sequences:
        start = min(start, seq.frame_start)
        end = max(end, seq.frame_final_end) - 1

    scene.use_preview_range = True
    scene.frame_preview_start, scene.frame_preview_end = (
        start,
        end,
    )


def emphasize_drawings(holder, context):
    """Emphasize drawings by changing the color balance of visible sequences.

    As the visible sequences are metasequences, it's required to get the movie sequences inside them, else the adv. drawings are modified too.

    :param holder: Current Blender property's holder
    :param context: Current context
    """
    sequence_editor = bpy.data.scenes["Scene"].sequence_editor
    current_sequence = sequence_editor.active_strip

    # Sentinel if Main scene, it makes blender crash...  TODO: Check regularily if fixed, hard to reproduce and make an issue
    if context.scene is bpy.data.scenes["Scene"]:
        for seq in sequence_editor.sequences:
            media_sequence = get_media_sequence(seq)

            # Sentinel for non graphic sequences
            if not media_sequence or media_sequence.type not in {"IMAGE", "MOVIE"}:
                continue

            # Create color balance modifier if doesn't exist
            modifier = media_sequence.modifiers.get("emphasize")
            if not modifier:
                color_balance = media_sequence.modifiers.new(
                    "emphasize", "COLOR_BALANCE"
                ).color_balance
            else:
                color_balance = modifier.color_balance

            # Change color balance value to make the sequences whiter
            value = ((holder.emphasize_drawings - 0) / (100 - 0)) * (2.0 - 1.0) + 1.0
            color_balance.lift = (value, value, value)

    # Set current sequence as active back
    sequence_editor.active_strip = current_sequence

    # Update Adv. Drawing background image display and alpha
    camera = bpy.data.cameras["Camera"]
    bg_img = camera.background_images.values()[0]
    bg_img.alpha = (100 - holder.emphasize_drawings) / 100


def compare_refresh(compare_properties, context):
    """Refresh compare.

    Hack until Blender VSE refresh is optimized.
    """

    seq_ed = context.scene.sequence_editor
    current_sequence = seq_ed.active_strip

    # Alpha
    values = [
        compare_properties.opacity if seq.channel == current_sequence.channel else 1
        for seq in seq_ed.sequences
    ]
    seq_ed.sequences.foreach_set("blend_alpha", values)

    # Refresh the sequencer TODO: Report this as setting alpha doesn't invalidate the cache
    bpy.ops.sequencer.refresh_all()


def add_wipe_effect(
    sequence: Union[MovieSequence, MetaSequence, ImageSequence]
) -> Tuple[WipeSequence, MetaSequence]:
    """Add wipe effect to the given sequence.

    Create an empty text sequence and a wipe, and stack them into a meta sequence.
    Add drivers to wipe parameters.

    :param sequence: Sequence to add wipe to
    :return: Created wipe sequence, meta sequence containing wipe sequences structure
    """
    # Abort if not correct
    if sequence.type not in ["IMAGE", "MOVIE"]:
        return

    scene = bpy.context.scene
    wipe_sequence = None

    # If not meta sequence create one
    metas_hierarchy = get_metas_hierarchy(sequence)
    parent_stack = metas_hierarchy[-1]

    # Make sure parent meta is a substitute
    if (
        type(parent_stack) is SequenceEditor
        or not parent_stack.get("stax_kind") == "substitute"
    ):
        parent_stack = build_substitute(parent_stack, sequence)

    # Set stax meta transparent blending
    parent_stack.blend_type = "ALPHA_OVER"

    # Substitute transform sequence to sequence if there is one
    for seq in parent_stack.sequences:
        if seq.type == "TRANSFORM" and seq.input_1 == sequence:
            sequence = seq
            break

    # Create empty text for transparency
    # --------------
    text_sequence = parent_stack.sequences.new_effect(
        name=f"{sequence.name}.text_wipe_holder",
        type="TEXT",
        channel=sequence.channel + 1,
        frame_start=sequence.frame_start,
        frame_end=sequence.frame_final_end,
    )
    text_sequence.text = ""
    text_sequence.blend_type = "ALPHA_OVER"

    # Create wipe effect
    # ------------------
    wipe_sequence = parent_stack.sequences.new_effect(
        name=f"{sequence.name}.wipe_effect",
        type="WIPE",
        channel=sequence.channel + 2,
        frame_start=sequence.frame_start,
        seq1=text_sequence,
        seq2=sequence,
    )

    # Disable default fade to be able to control the wipe
    wipe_sequence.use_default_fade = False
    wipe_sequence.effect_fader = 1

    # Reference source media sequence
    wipe_sequence["media_sequence_name"] = sequence.name

    # Add driver for wipe
    # ---------------------

    # NB: Wipe fader driver is not added there because not always active

    # Wipe angle
    add_single_driver(
        wipe_sequence,
        "angle",
        "SCENE",
        scene,
        "compare_properties.wipe_angle",
    )

    # Wipe blur
    add_single_driver(
        wipe_sequence,
        "blur_width",
        "SCENE",
        scene,
        "compare_properties.wipe_blur",
    )
    # ---------------------

    # Add wipe reference for fast catch
    sequence["wipe"] = wipe_sequence.name

    return wipe_sequence


def add_single_driver(target, data_name: str, id_type: str, id_obj, data_path: str):
    # Add driver
    fader_driver = target.driver_add(data_name).driver
    fader_driver.type = "AVERAGE"

    # Create variable
    var_name = data_path.replace(".", "__")
    fader_var = fader_driver.variables.get(var_name)
    if not fader_var:
        fader_var = fader_driver.variables.new()
        fader_var.name = var_name
    fader_var.type = "SINGLE_PROP"

    # Set variable parameters
    fader_var.targets[0].id_type = id_type
    fader_var.targets[0].id = id_obj
    fader_var.targets[0].data_path = data_path


def append_to_object_key(bl_object, item_key: str, item):
    """Append item (blender object) to a blender object under a key.

    Equivalent to bl_object[item_key].append(item)

    :param bl_object: Blender object to append the element to
    :param item_key: Key to append the element to
    :param item: Blender object to append
    """
    # Associate to sequence
    if not bl_object.get(item_key):
        # NB: Python's "set" is not a possible type, forced to use a list
        bl_object[item_key] = []

    # NB: Cannot use list.append()
    existing_objects = list(bl_object.get(item_key))
    bl_object[item_key] = [o for o in existing_objects if o] + [item]
