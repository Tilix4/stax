# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####
"""
Every operator relative to sequencer UI
"""

import bpy
from bpy.props import BoolProperty, EnumProperty

from stax.utils.utils_timeline import (
    get_media_sequence,
    get_review_sequence,
    get_reviewable_media_sequences,
)


class WM_OT_split_view(bpy.types.Operator):
    """Toggle Split sequencer view"""

    bl_idname = "wm.splitview"
    bl_label = "Split View"

    def invoke(self, context, event):
        if event.shift:
            context.window.screen = bpy.data.screens["Scripting"]
            return {"FINISHED"}

        sq_preview_areas = [
            x
            for x in context.screen.areas
            if x.type == "SEQUENCE_EDITOR" and x.spaces[0].view_type == "PREVIEW"
        ]
        if len(sq_preview_areas) == 1:
            # Split area
            bpy.ops.screen.area_split(
                {"area": sq_preview_areas[0]}, direction="VERTICAL"
            )

            # Disable toolbar on right panel
            # Update areas
            sq_preview_areas = [
                x
                for x in context.screen.areas
                if x.type == "SEQUENCE_EDITOR" and x.spaces[0].view_type == "PREVIEW"
            ]

            # Hide toolbar
            sq_preview_areas[0].spaces[0].show_region_toolbar = False

        else:
            # Give the remaining area to have the left area's channel displayed
            sq_preview_areas[0].spaces[0].display_channel = (
                sq_preview_areas[-1].spaces[0].display_channel
            )

            # Show toolbar of remaning area
            sq_preview_areas[0].spaces[0].show_region_toolbar = True

            # Join areas
            # Simulates the mouse position as being between the two areas horizontally
            # and a bit above the bottom corner vertically
            cursor_x = int(
                sq_preview_areas[0].x
                - (sq_preview_areas[0].x - sq_preview_areas[1].width) / 2
            )
            cursor_y = sq_preview_areas[0].y + 10
            bpy.ops.screen.area_join(
                cursor=(
                    cursor_x,
                    cursor_y,
                )
            )

            # Force UI update, due to Blender bug, delete when fixed -> https://developer.blender.org/T65529
            bpy.ops.screen.area_swap(
                cursor=(sq_preview_areas[0].x, sq_preview_areas[0].y)
            )
            bpy.ops.screen.area_swap(
                cursor=(sq_preview_areas[0].x, sq_preview_areas[0].y)
            )

        return {"FINISHED"}


class WM_OT_mouse_location(bpy.types.Operator):
    """This operator shows the mouse location when left clicking

    TODO Maybe create a ops_dev.py to store development utils

    For debug only. Run it with the operator search function F3"""

    bl_idname = "wm.mouse_position"
    bl_label = "Mouse location"

    def modal(self, context, event):
        if event.type == "LEFTMOUSE":
            print(f"x: {event.mouse_x}, y: {event.mouse_y}")
        return {"PASS_THROUGH"}

    def execute(self, context):
        context.window_manager.modal_handler_add(self)
        return {"RUNNING_MODAL"}


class WM_OT_DetachView(bpy.types.Operator):
    """Create a detached window of the current sequencer view"""

    bl_idname = "wm.detachview"
    bl_label = "Detach View"

    def execute(self, context):
        sq_preview_areas = [
            x
            for x in context.screen.areas
            if x.type == "SEQUENCE_EDITOR" and x.spaces[0].view_type == "PREVIEW"
        ]
        if sq_preview_areas:
            bpy.ops.screen.area_dupli(
                {"area": sq_preview_areas[-1]}, "INVOKE_DEFAULT"
            )  # TODO : unexpected arg ?
        return {"FINISHED"}


class SEQUENCER_OT_display_reviews_drawings(bpy.types.Operator):
    """Display reviews drawings of selected sequences"""

    bl_idname = "sequencer.display_reviews_drawings"
    bl_label = "Show Sequences Reviews Drawings"
    bl_options = {"REGISTER", "UNDO"}

    action: EnumProperty(
        name="Action",
        items=(
            (
                "SHOW",
                "Show reviews drawings",
                "Show reviews drawings of selected sequences",
            ),
            (
                "HIDE",
                "Hide reviews drawings",
                "Hide reviews drawings of selected sequences",
            ),
        ),
    )
    all: BoolProperty(name="All", description="Show all reviews drawings")

    @classmethod
    def poll(cls, context):
        if context.scene.sequence_editor.sequences:
            return True

    def invoke(self, context, event):
        # Key attributes
        self.all = event.shift

        return self.execute(context)

    def execute(self, context):
        # Sequences to show reviews drawings
        reviewable_sequences = (
            get_reviewable_media_sequences()
            if self.all
            else [get_media_sequence(seq) for seq in context.selected_sequences]
        )
        selected_sequences = [
            seq for seq in reviewable_sequences if seq.get("review_sequence_name")
        ]

        for sequence in selected_sequences:
            media_sequence = get_media_sequence(sequence)
            review_sequence = get_review_sequence(media_sequence)

            # Display action review related metasequence
            if self.action == "SHOW":
                if review_sequence.mute:  # Optim
                    review_sequence.mute = False

            elif self.action == "HIDE":
                if not review_sequence.mute:  # Optim
                    review_sequence.mute = True

        return {"FINISHED"}


class SEQUENCER_OT_toggle_annotations_keyframes(bpy.types.Operator):
    """Toggle annotations keyframes editing"""

    bl_idname = "sequencer.toggle_annotations_keyframes"
    bl_label = "Toggle Annotations Keyframes"
    bl_options = {"REGISTER", "UNDO"}

    # TODO make it simplier with https://gitlab.com/superprod/stax/-/issues/154

    @classmethod
    def poll(cls, context):
        if context.workspace is bpy.data.workspaces["Main"]:
            return True

    def execute(self, context):
        sequencer_area = [
            x
            for x in context.screen.areas
            if x.type == "SEQUENCE_EDITOR" and x.spaces[0].view_type == "SEQUENCER"
        ][0]
        sq_dopesheet_areas = [
            x
            for x in context.screen.areas
            if x.type == "DOPESHEET_EDITOR" and x.spaces[0].ui_mode == "GPENCIL"
        ]

        if not sq_dopesheet_areas:
            # Split area
            bpy.ops.screen.area_split({"area": sequencer_area}, direction="HORIZONTAL")

            # Update and get created areas
            keyframes_area = [
                x
                for x in context.screen.areas
                if x.type == "SEQUENCE_EDITOR" and x.spaces[0].view_type == "SEQUENCER"
            ][0]
            sequencer_area = [
                x
                for x in context.screen.areas
                if x.type == "SEQUENCE_EDITOR" and x.spaces[0].view_type == "SEQUENCER"
            ][1]

            # Change to display keyframes
            keyframes_area.type = "DOPESHEET_EDITOR"

            # Set synchronized time between areas
            for space in sequencer_area.spaces:
                space.show_locked_time = True
            for space in keyframes_area.spaces:
                space.show_locked_time = True
        else:
            # Join areas
            # Simulates the mouse position as being between the two areas vertically
            # and a bit above the bottom corner horizontally
            cursor_x = int(sq_dopesheet_areas[-1].width / 2)
            cursor_y = int(sq_dopesheet_areas[-1].y - 3)
            bpy.ops.screen.area_join(
                cursor=(
                    cursor_x,
                    cursor_y,
                )
            )

            # Force UI update, due to Blender bug, delete when fixed -> https://developer.blender.org/T65529
            sq_preview_areas = [
                x
                for x in context.screen.areas
                if x.type == "SEQUENCE_EDITOR" and x.spaces[0].view_type == "PREVIEW"
            ]
            bpy.ops.screen.area_swap(
                cursor=(sq_preview_areas[0].x, sq_preview_areas[0].y)
            )
            bpy.ops.screen.area_swap(
                cursor=(sq_preview_areas[0].x, sq_preview_areas[0].y)
            )

            # Set to sequencer back, else the dopesheet remains
            sq_dopesheet_areas = [
                x
                for x in context.screen.areas
                if x.type == "DOPESHEET_EDITOR" and x.spaces[0].ui_mode == "GPENCIL"
            ]
            sq_dopesheet_areas[0].type = "SEQUENCE_EDITOR"

            # Move the tool header to top back, else it stays on the bottom by default
            sequencer_area = [
                x
                for x in context.screen.areas
                if x.type == "SEQUENCE_EDITOR" and x.spaces[0].view_type == "SEQUENCER"
            ][0]

            bpy.ops.screen.region_flip(
                {
                    "area": sequencer_area,
                    "region": sequencer_area.regions[0],
                }
            )

        return {"FINISHED"}


class SEQUENCER_OT_toggle_meta(bpy.types.Operator):
    """This operator substitutes sequencer.meta_toggle to exit the active meta if it's a substitute"""

    # TODO Use API when available to override sequencer.meta_toggle directly.

    bl_idname = "sequencer.toggle_meta"
    bl_label = "Toggle Meta"

    @classmethod
    def poll(cls, context):
        sequence_editor = context.scene.sequence_editor
        return (
            sequence_editor.active_strip
            and sequence_editor.active_strip.type == "META"
            or sequence_editor.meta_stack
        )

    def execute(self, context):
        sequence_editor = context.scene.sequence_editor
        current_sequence = sequence_editor.active_strip

        # Exit current meta if active meta is substitute
        if current_sequence and current_sequence.get("stax_kind"):
            sequence_editor.active_strip = None

        bpy.ops.sequencer.meta_toggle()

        return {"RUNNING_MODAL"}


classes = [
    WM_OT_mouse_location,
    WM_OT_split_view,
    WM_OT_DetachView,
    SEQUENCER_OT_display_reviews_drawings,
    SEQUENCER_OT_toggle_annotations_keyframes,
    SEQUENCER_OT_toggle_meta,
]


def register():
    for cls in classes:
        bpy.utils.register_class(cls)


def unregister():
    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)
